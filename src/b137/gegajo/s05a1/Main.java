package b137.gegajo.s05a1;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("Session 5 Activity");

//        Contact firstPerson = new Contact();
//
////        Scanner appScanner = new Scanner(System.in);
////        System.out.println("Please enter name: ");
////        String name = appScanner.nextLine();
//        String name = ("Roselle Gegajo");
//        firstPerson.setName(name);
//
////        System.out.println("Please enter contact number: ");
////        String number = appScanner.nextLine();
//        ArrayList<String> number = new ArrayList<String>();
//        number.add("0912345678");
//        number.add("0987654322");
//        for(String numbers : number)
//        firstPerson.setNumbers(numbers);
//
////        System.out.println("Please enter address: ");
////        String address = appScanner.nextLine();
//        ArrayList<String> address = new ArrayList<String>();
//        address.add("123, Anonas Street Manila City");
//        address.add("456, Larva Street Taguig City");
//        for(String addresses : address)
//        firstPerson.setAddresses(addresses);
//
//        firstPerson.showName();
//        firstPerson.showDetailsOfNumbers();
//        firstPerson.showDetailsOfAddresses();

        System.out.println("=============");

        Phonebook list = new Phonebook();

        String name2 = ("Kim Taehyung");
        list.setName(name2);

        ArrayList<String> number2 = new ArrayList<String>();
        number2.add("0999999999");
        number2.add("0944444444");
        for(String numbers : number2)
            list.setNumbers(numbers);

        ArrayList<String> address2 = new ArrayList<String>();
        address2.add("I live in Seoul Korean");
        address2.add("I work at Hybe");
        for(String addresses : address2)
            list.setAddresses(addresses);

        list.message();

        System.out.println("=============");

        Phonebook list2 = new Phonebook();

        String name3 = ("Park Jimin");
        list2.setName(name3);

        ArrayList<String> number3 = new ArrayList<String>();
        number3.add("0988888888");
        number3.add("0922222222");
        for(String numbers : number3)
            list2.setNumbers(numbers);

        ArrayList<String> address3 = new ArrayList<String>();
        address3.add("I live in Busan Korean");
        address3.add("I work at Big Hit");
        for(String addresses : address3)
            list2.setAddresses(addresses);

        list2.message();

    }
}
