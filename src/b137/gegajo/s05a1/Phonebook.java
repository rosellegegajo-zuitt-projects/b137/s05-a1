package b137.gegajo.s05a1;

import java.util.ArrayList;

public class Phonebook extends Contact {
    //Properties
    private ArrayList<String> PhonebookList = new ArrayList<String>();

    //Constructor
    public Phonebook() { super(); }

    public Phonebook(String name, ArrayList<String> numbers, ArrayList<String> addresses, ArrayList<String> PhonebookList){
        super(name, numbers, addresses);
        this.PhonebookList = PhonebookList;
    }

    //Methods
    public void message() {
        if(super.getName() == null){
            System.out.println("Empty, Please see Phonebook List");
        } else {
            showName();
            showDetailsOfNumbers();
            showDetailsOfAddresses();
        }
    }

}
