package b137.gegajo.s05a1;

import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    //Constructor
    public Contact(){}

    //Parameterized
    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses){
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    //Getter
    public String getName() {
        return name;
    }

    public ArrayList<String> getNumbers(){
        return numbers;
    }

    public ArrayList<String> getAddresses(){
        return addresses;
    }

    //Setters
    public void setName(String newName){
        this.name = newName;
    }
    public void setNumbers(String newNumbers){
        numbers.add(newNumbers);
    }
    public void setAddresses(String newAddresses){
        addresses.add(newAddresses);
    }

    //Methods
    public void showName(){
        System.out.println(this.name);
    }

    public void showDetailsOfNumbers(){
        System.out.println("---------");
        System.out.println(this.name + " has the following registered numbers: ");
        for(String number : numbers) {
            System.out.println(number);
        }
    }
    public void showDetailsOfAddresses(){
        System.out.println("---------");
        System.out.println(this.name + " has the following registered addresses: ");
        for(String address : addresses){
            System.out.println(address);
        }
    }

}
